package br.com.alura.nnascimento.forum.controller;

import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.alura.nnascimento.forum.controller.dto.DetalhesTopicoDTO;
import br.com.alura.nnascimento.forum.controller.dto.TopicoDTO;
import br.com.alura.nnascimento.forum.controller.form.AtualizacaoTopicoForm;
import br.com.alura.nnascimento.forum.controller.form.TopicoForm;
import br.com.alura.nnascimento.forum.modelo.Topico;
import br.com.alura.nnascimento.forum.repository.CursoRepository;
import br.com.alura.nnascimento.forum.repository.TopicoRepository;

// Notacao que faz com que todos os metodos funcionem como se estivem usando ResponseBody (retorno do metodo deve ser exibido no navegador)
@RestController
// Fazendo com que a classe responda ao path abaixo, cada metodo é chamado de acordo com o seu metodo
@RequestMapping("/topicos")
public class TopicosController {
	
	//Injetando o acesso aos metodos CRUD do banco
	@Autowired
	private TopicoRepository topicoRepository;
	
	//Injetando o acesso aos metodos CRUD do banco
	@Autowired
	private CursoRepository cursoRepository;
	
	// Notacao que cria o endpoint e identifica o seu path e o método chamado
	// @RequestMapping(method = RequestMethod.GET, path =  "/topicos")
	//metodo de leitura de recurso
	@GetMapping
	@Cacheable(value = "listaDeTopicos")
	public Page<TopicoDTO> lista(@RequestParam(required = false) String nomeCurso, @PageableDefault(sort = "mensagem", direction = Direction.DESC) Pageable paginacao) {
		
		if (nomeCurso == null) {
			
			// Carregando todos os topicos
			Page<Topico> topico = topicoRepository.findAll(paginacao);
			// Metodo que retorna uma lista com os objetos passados como parametro
			return TopicoDTO.converter(topico);
		
		} else {
			
			Page<Topico> topico = topicoRepository.findByCursoNome(nomeCurso, paginacao);
			return TopicoDTO.converter(topico);
			
		}
		
	}
	
	// Metodo de criacao de recurso
	@PostMapping
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true)
	public ResponseEntity<TopicoDTO> cadastrar(@RequestBody @Valid TopicoForm form, UriComponentsBuilder uriBuilder){
		Topico topico = form.converterTopico(cursoRepository);
		topicoRepository.save(topico);
		
		// uriBuilder tras o path completo ({id} atributo dinamico a ser substituido no buildAndExpand)
		URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(topico.getId()).toUri();
		
		// Devolvendo code 201 (novo recurso criado com sucesso). Deve ser passado uma URI (Path do recurso) + representacao do recurso
		return ResponseEntity.created(uri).body(new TopicoDTO(topico));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<DetalhesTopicoDTO> detalhar(@PathVariable Long id) {
		
		Optional<Topico> topico = topicoRepository.findById(id);
		//Validando se o id passado como parametro existe
		return topico.isPresent() ? ResponseEntity.ok(new DetalhesTopicoDTO(topico.get())) : ResponseEntity.notFound().build();
		
	}
	
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<TopicoDTO> atualizar(@PathVariable Long id, @RequestBody @Valid AtualizacaoTopicoForm form){
		
		Optional<Topico> optional = topicoRepository.findById(id);
		if (optional.isPresent()) {
			Topico topico = form.atualizar(id,topicoRepository);
			return ResponseEntity.ok(new TopicoDTO(topico));
		}
		
		return ResponseEntity.notFound().build();
		
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true)
	public ResponseEntity<?> remover(@PathVariable Long id){
		
		Optional<Topico> optional = topicoRepository.findById(id);
		if(optional.isPresent()) {
			topicoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();	
			
	}
	
}
