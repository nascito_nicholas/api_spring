package br.com.alura.nnascimento.forum.config.validacao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

// Para tratamentos de erros
@RestControllerAdvice
public class ErroValidacaoHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	//Dizendo que o Spring deve voltar o erro 400
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	// Dizendo que toda vez que houver uma excecao esse metodo deve ser chamado
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ErroDTO> handle(MethodArgumentNotValidException exception) {
		
		List<ErroDTO> dto = new ArrayList<>();
		List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
		
		fieldErrors.forEach(e -> {
			String mensagem = messageSource.getMessage(e, LocaleContextHolder.getLocale());
			ErroDTO erro = new ErroDTO(e.getField(), mensagem);
			dto.add(erro);
		});
		return dto;
		
	}
	
}
