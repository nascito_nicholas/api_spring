package br.com.alura.nnascimento.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.alura.nnascimento.forum.modelo.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long>{

	Curso findByNome(String nomeCurso);
}
