package br.com.alura.nnascimento.forum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.alura.nnascimento.forum.modelo.Topico;

public interface TopicoRepository extends JpaRepository<Topico, Long>{

	Page<Topico> findByCursoNome(String nomeCurso, Pageable paginacao);
	
	// Caso tenha um cenario de ambiguiidade, exemplo, um atributo cursoNome nesse caso utilizar underscore para especificar que e por relacionamento
	//List<Topico> findByCurso_Nome(String nomeCurso);
	
	// Alternativa para não seguir o padrao de nomenclatura (Query JPQL)
	//@Query("select * from topico t where t.curso.nome = :nomeCurso")
	//List<Topico> carregarPorNomeCurso(@Param("nomeCurso") String nomeCurso);
	

}
